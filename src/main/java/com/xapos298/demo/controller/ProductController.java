package com.xapos298.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.repository.ProductRepository;

@Controller
@RequestMapping("/Product")
public class ProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("Product/indexapi");
		return view;
	}
}
