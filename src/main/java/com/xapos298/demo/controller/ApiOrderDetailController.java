package com.xapos298.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.OrderDetail;
import com.xapos298.demo.model.category;
import com.xapos298.demo.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderDetailController {
	@Autowired
	public OrderDetailRepository orderDetailRepository;
	
	@PostMapping("orderdetail/add")
	public ResponseEntity<Object> saveOrderDeteail(@RequestBody OrderDetail orderDetail){
	
		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		
		if (orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<>("Save Item Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderList/{headerId}")
	public ResponseEntity<List<OrderDetail>> getAllOrderDetail(@PathVariable("headerId") Long headerId){
		try {
			List<OrderDetail> listOrder = this.orderDetailRepository.findByHeaderId(headerId);
			if (listOrder.equals(listOrder)) {
				ResponseEntity rest = new ResponseEntity(listOrder, HttpStatus.OK);
				return rest;
				//return new ResponseEntity<>("Save Succesfully", HttpStatus.OK);
			} else {
				return ResponseEntity.notFound().build();
				//return new ResponseEntity<>("Save Failed", HttpStatus.NO_Content);
			}
		} catch (Exception e) {
			return new ResponseEntity<List<OrderDetail>>(HttpStatus.NO_CONTENT);
			//return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
}
