package com.xapos298.demo.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Variants;
import com.xapos298.demo.model.category;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.VariantsRepository;

@Controller
@RequestMapping("/Variants/")
public class VariantsController {
	boolean isEdit = false;
	@Autowired
	private VariantsRepository variantsRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("Variants/indexapi");
		return view;
	}

	@GetMapping("index")
	public ModelAndView index(@ModelAttribute Variants variants) {
		ModelAndView view = new ModelAndView("/Variants/index");
		List<Variants> listVariants = this.variantsRepository.findByIsActive(true);
		view.addObject("listVariants", listVariants);
		return view;
	}
	
	
	@GetMapping("addvariants")
	public ModelAndView addvariants() {
		ModelAndView view = new ModelAndView("/Variants/addvariants");
		Variants variants = new Variants();
		view.addObject("Variants", variants);
		//ambil category list dari tabel category
		List<category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		return view;
	}
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variants variants, BindingResult result) { //untuk menirim data dari controler ke database
		if(!result.hasErrors()) {
			if(isEdit == true) {
				Variants oldVariants = this.variantsRepository.findById(variants.getId()).orElse(null);
				oldVariants.setModifyBy("root");
				oldVariants.setModifyDate(new Date());
				oldVariants.setVariantsInitial(variants.getVariantsInitial());
				oldVariants.setVariantsName(variants.getVariantsName());
				oldVariants.setIsActive(variants.getIsActive());
				variants = oldVariants;
				isEdit = false;
			}
			else {
				variants.setCreateBy("Alif");
				variants.setCreateDate(new Date());			
			}
			this.variantsRepository.save(variants);
		}
		return new ModelAndView("redirect:/Variants/index");
		
	}
	
	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id ) {
		ModelAndView view = new ModelAndView("Variants/addvariants");		
		Variants variants = this.variantsRepository.findById(id).orElse(null);		
		view.addObject("Variants", variants);
		//ambil category list dari tabel category
		List<category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		isEdit=true;
		return view;
	}
	@GetMapping("delete/{ids}")
	public ModelAndView delete(@PathVariable("ids") Long id ) {
		if(id != null) {
			this.variantsRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/Variants/index");
	}
}
