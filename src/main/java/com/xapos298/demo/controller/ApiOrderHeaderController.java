package com.xapos298.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.OrderDetail;
import com.xapos298.demo.model.OrderHeader;
import com.xapos298.demo.model.category;
import com.xapos298.demo.repository.OrderHeaderRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderHeaderController {
	
	@Autowired 
	public OrderHeaderRepository orderHeaderRepository;
	
	@PostMapping("orderHeader/add")
	public ResponseEntity<Object> createReferene(@RequestBody OrderHeader orderHeader){
		String timeDec = System.currentTimeMillis()+ "";
		orderHeader.setReference(timeDec);
		orderHeader.setAmount(0 + "");
		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		
		if (orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<>("Create Order Header", HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>("Create Failed", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("maxorderheaderid")
	public ResponseEntity<Long> getMaxOrderHeaderId(){
		try {
			Long maxId = this.orderHeaderRepository.findByMaxId();
			return new ResponseEntity<Long>(maxId, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("orderHeader/done")
	public ResponseEntity<Object> doneProccess(@RequestBody OrderHeader orderHeader){
		//ambil id dari requestbody orderHeader
		Long id = orderHeader.getId();
		
		Optional<OrderHeader> orderHeaderData = this.orderHeaderRepository.findById(id);
		if(orderHeaderData.isPresent()) {
			orderHeader.setId(id); //set id dari id orderHeader di atas
			this.orderHeaderRepository.save(orderHeader);
			return new ResponseEntity<Object>("Order Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("lastOrder")
	public ResponseEntity<List<OrderHeader>> getAllOrderHeader(){
		try {
			List<OrderHeader> listOrderHeader = this.orderHeaderRepository.findByAmountNotNull();
			return new ResponseEntity<List<OrderHeader>>(listOrderHeader, HttpStatus.OK);			
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	
}
