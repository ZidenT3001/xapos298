package com.xapos298.demo.controller;


import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.jaxb.SpringDataJaxb.PageRequestDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Variants;
import com.xapos298.demo.model.category;
import com.xapos298.demo.repository.VariantsRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiVariantsController {
	@Autowired
	public VariantsRepository variantsRepository;
	
	@GetMapping("Variants")	
	public ResponseEntity<List<Variants>> getALLVariants(){
		try {
			List<Variants> listVariants = this.variantsRepository.findByIsActive(true);
			return new ResponseEntity<List<Variants>>(listVariants, HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);		
		}
	}
	
	
	@GetMapping("Variants/{id}")
	public ResponseEntity<List<Variants>> getVariantsById(@PathVariable("id") Long id){
		try {
			Optional<Variants> variants = this.variantsRepository.findById(id);
			if (variants.isPresent()) {
				ResponseEntity rest = new ResponseEntity(variants, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Variants>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("VariantsByCategory/{categoryId}")
	public ResponseEntity<List<Variants>> getAllVariants(@PathVariable("categoryId") Long categoryId){
		try {
			List<Variants> listVariants = this.variantsRepository.findByCategoryId(categoryId);
			return new ResponseEntity<List<Variants>>(listVariants, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("Variants/add")
	public ResponseEntity<Object> saveVariants(@RequestBody Variants variants){
		variants.setCreateBy("Raphael");
		variants.setCreateDate(new Date());
		Variants variantsData = this.variantsRepository.save(variants);
		if(variantsData.equals(variants)) {
			return new ResponseEntity<>("save data successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("save failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@PutMapping("edit/Variants/{id}")
	public ResponseEntity<Object> editVariants(@PathVariable("id") Long id, @RequestBody Variants variants) {
		Optional<Variants> variantsData = this.variantsRepository.findById(id);
		if (variantsData.isPresent()) {
			variants.setId(id);
			variants.setModifyBy("root");
			variants.setModifyDate(Date.from(Instant.now()));
			variants.setCreateBy(variantsData.get().getCreateBy());
			variants.setCreateDate(variantsData.get().getCreateDate());
			this.variantsRepository.save(variants);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// dalete category
	@PutMapping("delete/Variants/{id}")
	public ResponseEntity<Object> deleteVariants(@PathVariable("id") Long id) {
		Optional<Variants> variantsData = this.variantsRepository.findById(id);
		if (variantsData.isPresent()) {
			Variants variants = new Variants();
			variants.setId(id);
			variants.setIsActive(false);
			variants.setModifyBy("root");
			variants.setModifyDate(Date.from(Instant.now()));
			variants.setCreateBy(variantsData.get().getCreateBy());
			variants.setCreateDate(variantsData.get().getCreateDate());
			// category.setCategoryCode(categoryData.get().getCategoryCode());
			variants.setVariantsName(variantsData.get().getVariantsName());
			this.variantsRepository.save(variants);
			return new ResponseEntity<>("Deleted Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("Variants/pagging")
	public ResponseEntity<Map<String, Object>> getAllVariants(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size){
		try {
			List<Variants> variants = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<Variants> pageTuts;
			pageTuts = this.variantsRepository.findAll(pagingSort);
			
			variants = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("variants", variants);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
						
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
