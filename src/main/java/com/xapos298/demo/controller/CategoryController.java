package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Variants;
import com.xapos298.demo.model.category;
import com.xapos298.demo.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {
	boolean isEdit = false;
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("category/indexapi");
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index");
		List<category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		//System.out.println(listCategory.get(0).getCategoryName());
		return view;
	}
	
	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("category/addform");
		category category = new category();
		view.addObject("category", category);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute category category, BindingResult result) { //untuk menirim data dari controler ke database
		if(!result.hasErrors()) {
			if(isEdit == true) {
				category oldCategory = this.categoryRepository.findById(category.getId()).orElse(null);
				oldCategory.setModifyBy("root");
				oldCategory.setModifyDate(new Date());
				oldCategory.setCategoryInitial(category.getCategoryInitial());
				oldCategory.setCategoryName(category.getCategoryName());
				oldCategory.setIsActive(category.getIsActive());
				category = oldCategory;
				isEdit = false;
			}
			else {
				category.setCreateBy("Alif");
				category.setCreateDate(new Date());			
			}
			this.categoryRepository.save(category);
		}
		return new ModelAndView("redirect:/category/index");
		
	}
	
	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id) {
		ModelAndView view = new ModelAndView("category/addform");		
		category category = this.categoryRepository.findById(id).orElse(null);		
		view.addObject("category", category);
		isEdit = true;
		return view;
	}
	@GetMapping("delete/{ids}")
	public ModelAndView delete(@PathVariable("ids") Long id ) {
		if(id != null) {
			this.categoryRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/category/index");
	}
	
}
