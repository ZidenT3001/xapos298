package com.xapos298.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Optional;

import com.xapos298.demo.model.OrderHeader;
import com.xapos298.demo.model.category;
import com.xapos298.demo.repository.CategoryRepository;

//membuat API
@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCategoryController {

	@Autowired
	public CategoryRepository categoryRepository;

	@GetMapping("category")
	public ResponseEntity<List<category>> getAllCategory() {
		try {
			List<category> listCategory = this.categoryRepository.findByIsActive(true);
			return new ResponseEntity<List<category>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	// save data dari client ke database
	@PostMapping("category/add")
	public ResponseEntity<Object> saveCategory(@RequestBody category category) {
		category.setCreateBy("Raphael");
		category.setCreateDate(new Date());
		category categoryData = this.categoryRepository.save(category);
		if (categoryData.equals(category)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	// get kategory by id
	@GetMapping("category/{id}")
	public ResponseEntity<List<category>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<category> category = this.categoryRepository.findById(id);
			if (category.isPresent()) {
				ResponseEntity rest = new ResponseEntity(category, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<category>>(HttpStatus.NO_CONTENT);
		}
	}

	// edit data category
	@PutMapping("edit/category/{id}")
	public ResponseEntity<Object> editCategory(@PathVariable("id") Long id, @RequestBody category category) {
		Optional<category> categoryData = this.categoryRepository.findById(id);
		if (categoryData.isPresent()) {
			category.setId(id);
			category.setModifyBy("root");
			category.setModifyDate(Date.from(Instant.now()));
			category.setCreateBy(categoryData.get().getCreateBy());
			category.setCreateDate(categoryData.get().getCreateDate());
			this.categoryRepository.save(category);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// dalete category
	@PutMapping("delete/category/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") Long id) {
		Optional<category> categoryData = this.categoryRepository.findById(id);
		if (categoryData.isPresent()) {
			category category = new category();
			category.setId(id);
			category.setIsActive(false);
			category.setModifyBy("Alif");
			category.setModifyDate(Date.from(Instant.now()));
			category.setCreateBy(categoryData.get().getCreateBy());
			category.setCreateDate(categoryData.get().getCreateDate());
			// category.setCategoryCode(categoryData.get().getCategoryCode());
			category.setCategoryName(categoryData.get().getCategoryName());
			this.categoryRepository.save(category);
			return new ResponseEntity<>("Deleted Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@GetMapping("categorySearch/{key}")
	public ResponseEntity<List<category>> searchCategory(@PathVariable("key") String key) {
		try {
			List<category> listCategory = this.categoryRepository.searchByKey(key);
			return new ResponseEntity<List<category>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
