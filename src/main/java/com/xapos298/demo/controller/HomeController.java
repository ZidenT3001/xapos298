package com.xapos298.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
	@GetMapping("form")
	public String form() {
		return "form";				
	}
	@GetMapping("Photo")
	public String photo() {
		return "1";
	}
	@GetMapping("style")
	public String css() {
		return "style";
	}
	@GetMapping("script")
	public String js() {
		return "script";
	}
	@GetMapping("FormProfile")
	public String FormProfile() {
		return "FormProfile";
	}
}
