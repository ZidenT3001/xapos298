package com.xapos298.demo.model;



import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column(name = "id")
	private Long id;
	@Column(name = "product_initial")
	private String productInitial;
	@Column(name = "product_name")
	private String productName;
	@Column(name = "product_description")
	private String productDescription;
	@Column(name = "product_price")
	private Integer productPrice;
	@Column(name = "product_stock")
	private Integer productStock;
	@Column(name = "is_active")
	private Boolean isActive;
	@Column(name = "create_by")
	private String createBy;
	@Column(name = "create_date")
	private Date createDate;
	@Column(name = "modify_by")
	private String modifyBy;
	@Column(name = "modify_date")
	private Date modifyDate;
	
	//FK
	@Column(name = "variants_id")
	private Long variantsId;
	
	//Join Many-to-One
	@ManyToOne
	@JoinColumn(name = "variants_id", insertable = false, updatable = false)
	private Variants variants;
	
	@ManyToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductInitial() {
		return productInitial;
	}

	public void setProductInitial(String productInitial) {
		this.productInitial = productInitial;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Integer getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}

	public Integer getProductStock() {
		return productStock;
	}

	public void setProductStock(Integer productStock) {
		this.productStock = productStock;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Long getVariantsId() {
		return variantsId;
	}

	public void setVariantsId(Long variantsId) {
		this.variantsId = variantsId;
	}

	public Variants getVariants() {
		return variants;
	}

	public void setVariants(Variants variants) {
		this.variants = variants;
	}
}
