package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.category;

public interface CategoryRepository extends JpaRepository<category, Long>{
	List<category> findByIsActive(boolean isActive);
	
	
	@Query(value = "SELECT * FROM category WHERE (lower(category_name) LIKE lower(concat('%', ?1, '%')))", nativeQuery = true)
	List<category> searchByKey(String key);
	//List<category> searchByKeyword(String keyword);
}

