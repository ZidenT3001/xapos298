package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos298.demo.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	List<Product> findByIsActive(boolean isActive);
}
