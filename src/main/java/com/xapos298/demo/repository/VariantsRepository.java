package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Variants;

public interface VariantsRepository extends JpaRepository<Variants, Long>{
	List<Variants> findByIsActive(boolean isActive);
	
//	//Menggunakan JpaRepository
//	List<Variants> findByIsActiveANDcreateBy(Boolean isActive, String user);
//	
//	//menggunakan Query Native
//	@Query(value = "SELECT * FROM Variants WHERE is_active = ?1 AND create_by = ?2")
//	List<Variants> find(Boolean isActive, String orang);
	List<Variants> findByCategoryId (Long categoryId);
}